<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#main-features">Main Features</a></li>
        <li><a href="#secondary-features">Secondary Features</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

Basic backend project for login and profile retrieval using nestjs.

### Main features
* Read and write data from database
* Create a new user profile
* Login using JWT
* Obtain profile given a JWT

### Secondary features
* Validation using custom pipes (e.g. `profile-validation.pipe.ts`)
* Creation of custom decorators (e.g. `get-user.decorator`)
* Use of Guards for login process and profile data retrieval
* Use nestjs build in logger (Use different log level like : log, warn and error)
* Encrypt user password
* Compare plain password with encripted password
* Use of environment variables `.env`
* Use of dto's and interfaces for data transfer
* Use of typed variables
* Responses well formated and with correct HTTP code (e.g. BadRequest, Unauthorized, etc)
* Do not use any ORM
* Use a single connection for database queries
* Database, tables and default data is created when application is started (only the first time)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- BUILD WITH -->
### Built With

* [NestJS](https://nestjs.org/)
* [MySql](https://mysql.com/)

Additional npm packages like:
* [bcrypt](https://www.npmjs.com/package/bcrypt)
* [mysql2](https://www.npmjs.com/package/mysql2)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

Instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

Nodejs and MySql (or MariaDB)

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/your_username_/Project-Name.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Setup environment variables according to your DB installation, modify `.env` if required
   ```js
    DATABASE_HOST=localhost
    DATABASE_PORT=3306
    DATABASE_USER=root
    DATABASE_PASSWORD=

    DATABASE_DBNAME=luckydb
   ```
4. Deploy application
   ```sh
   npm run start:dev
   ```
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

After application is deployed you can use any client (e.g. postman) to test existing end points.

By default applications runs in port 3000

### 1. Create a new profile
 * Verb: POST
 * Url: localhost:3000/profiles (replace localhost with correct hostname or ip)
 * Body (json):
    ```js
    {
        "username": "jdoe",
        "password": "secret",
        "name": "jhon",
        "address": "26th avenue",
        "cityId": 3
    }
    ```
  * Success Response (HttpCode: 201 Created)
    ```js
    {
      "id": 1,
      "name": "jhon",
      "address": {
        "street": "26th avenue",
        "city": "Cochabamba",
        "country": "Bolivia"
      }
    }
    ```

### 2. Login
 * Verb: POST
 * Path: localhost:3000/auth/login (replace localhost with correct hostname or ip)
 * Body (json):
    ```js
    {
        "username": "jdoe",
        "password": "secret",
    }
    ```
 * Success Response (HttpCode: 201 Created)
    ```js
    {
      "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2V..."
    }
    ```

### 3. Get a profile given a JWT
* Verb: GET
* Path: localhost:3000/profiles (replace localhost with correct hostname or ip)
* Header
 ** Key: Authorization
 ** Value: Bearer `GENERATED_TOKEN`
 
* Success Response (HttpCode: 200 Ok)
    ```js
    {
      "id": 1,
      "name": "jhon",
      "address": {
        "street": "26th avenue",
        "city": "Cochabamba",
        "country": "Bolivia"
      }
    }
    ```
<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

- Sider Huanca - [hs.sider@gmail.com](mailto:hs.sider@gmail.com)

<p align="right">(<a href="#top">back to top</a>)</p>
