import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common";
import { CreateProfileDto } from "src/profiles/dto/createProfileDto";

@Injectable()
export class ProfileValidationPipe implements PipeTransform<CreateProfileDto> {

    async transform( value: CreateProfileDto, { metatype }: ArgumentMetadata) {

        if (this.isEmpty(value.username)) {
            throw new BadRequestException('Validation failed (username cannot be empty)');
        }
        if (this.isEmpty(value.password)) {
            throw new BadRequestException('Validation failed (password cannot be empty)');
        }
        if (this.isEmpty(value.name)) {
            throw new BadRequestException('Validation failed (name cannot be empty)');
        }
        if (this.isEmpty(value.address)) {
            throw new BadRequestException('Validation failed (address cannot be empty)');
        }
        if (isNaN(value.cityId) || value.cityId < 0) {
            throw new BadRequestException('Validation failed (cityId must be a number greather than or equal to 0)');
        }
        return value;
    }

    isEmpty(str: string): boolean {
        return (!str || str.length === 0)
    }
}