import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DBService } from './mysql/db-service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
  DBService.init();
}
bootstrap();
