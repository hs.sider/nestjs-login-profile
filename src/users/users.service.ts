import { Injectable } from '@nestjs/common';
import { AuthCredentialsDto } from 'src/auth/dto/auth-credentials.dto';
import { DBService } from 'src/mysql/db-service';

@Injectable()
export class UsersService {

    async getUserByUsername(username: string): Promise<AuthCredentialsDto> {
        const [rows] = await DBService.connection.execute('SELECT * FROM `user` WHERE username = ?', [username]);
        return rows[0];
    }
}
