export class AuthCredentialsDto {
    id: number;
    username: string;
    password: string;
}