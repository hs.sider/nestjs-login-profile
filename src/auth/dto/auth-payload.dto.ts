export class AuthPayloadDto {
    id: number;
    username: string;
}