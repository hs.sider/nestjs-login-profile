import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import * as bcrypt from 'bcrypt';
import { AuthPayloadDto } from './dto/auth-payload.dto';

@Injectable()
export class AuthService {

    private readonly logger = new Logger(AuthService.name);

    constructor(private jwtService: JwtService, private userService: UsersService) { }

    async validateUser(username: string, pass: string): Promise<AuthPayloadDto> {
        const user = await this.userService.getUserByUsername(username);

        if (user && await bcrypt.compare(pass, user.password)) {
            const { password, ...result } = user;
            return result;
        }
        this.logger.warn(`Invalid username/password for user '${username}'`);
        return null;
    }

    async login(user: AuthCredentialsDto) {
        const payload = { username: user.username, sub: user.id };
        return {
            access_token: this.jwtService.sign(payload),
        }
    }
}
