import { Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { ProfilesService } from "src/profiles/profiles.service";
import { jwtConstants } from "./constant";
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    private readonly logger = new Logger(JwtStrategy.name);

    constructor(private profilesService: ProfilesService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.secret
        })
    }

    async validate(payload: JwtPayload) {
        const user = await this.profilesService.getProfileByUserId(payload.sub);
        if (!user) {
            this.logger.warn(`User validation fails`);
            throw new UnauthorizedException();
        }
        return user;
    }
}