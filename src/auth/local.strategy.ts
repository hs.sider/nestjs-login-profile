import { Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthService } from "./auth.service";
import { AuthPayloadDto } from "./dto/auth-payload.dto";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {

    private readonly logger = new Logger(LocalStrategy.name);

    constructor(private authService: AuthService) {
        super();
    }

    async validate(username: string, password: string): Promise<AuthPayloadDto> {
        const user = await this.authService.validateUser(username, password);
        if (!user) {
            this.logger.warn(`User validation fails, '${username}' is not a valid user`);
            throw new UnauthorizedException();
        }

        return user;
    }
}