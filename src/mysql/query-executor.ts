import { DBService } from "./db-service";

export class QueryExecutor {

    public static async execute(queries: string[]) {
        for (const query of queries) {
            await DBService.connection.execute(query, []);
        }
    }
}