export const CREATE_CITY_TABLE = `
  CREATE TABLE \`city\` (
    \`id\` int(11) NOT NULL AUTO_INCREMENT,
    \`countryId\` int(11) DEFAULT NULL,
    \`name\` varchar(45) DEFAULT NULL,
    PRIMARY KEY (\`id\`),
    KEY \`fk_country\` (\`countryId\`),
    CONSTRAINT \`fk_country\` FOREIGN KEY (\`countryId\`) REFERENCES \`country\` (\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  `;

export const POPULATE_CITY_TABLE = `
  INSERT INTO \`city\`
      (\`countryId\`, \`name\`)
  VALUES
      (1, 'Buenos Aires'),
      (1, 'Cordoba'),
      (2, 'Cochabamba'),
      (2, 'La Paz'),
      (3, 'Santiago'),
      (3, 'Arica');
  `;