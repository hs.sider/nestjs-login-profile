export const CREATE_USER_TABLE = `
  CREATE TABLE \`user\` (
    \`id\` int(11) NOT NULL AUTO_INCREMENT,
    \`username\` varchar(45) NOT NULL,
    \`password\` varchar(100) NOT NULL,
    PRIMARY KEY (\`id\`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  `;