export const CREATE_PROFILE_TABLE = `
  CREATE TABLE \`profile\` (
    \`id\` int(11) NOT NULL AUTO_INCREMENT,
    \`userId\` int(11) DEFAULT NULL,
    \`addressId\` int(11) DEFAULT NULL,
    \`name\` varchar(45) DEFAULT NULL,
    PRIMARY KEY (\`id\`),
    KEY \`fk_user\` (\`userId\`),
    KEY \`fk_address\` (\`addressId\`),
    CONSTRAINT \`fk_address\` FOREIGN KEY (\`addressId\`) REFERENCES \`address\` (\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT \`fk_user\` FOREIGN KEY (\`userId\`) REFERENCES \`user\` (\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  `;