export const CREATE_ADDRESS_TABLE = `
  CREATE TABLE \`address\` (
    \`id\` int(11) NOT NULL AUTO_INCREMENT,
    \`cityId\` int(11) DEFAULT NULL,
    \`street\` varchar(45) DEFAULT NULL,
    PRIMARY KEY (\`id\`),
    KEY \`fk_city\` (\`cityId\`),
    CONSTRAINT \`fk_city\` FOREIGN KEY (\`cityId\`) REFERENCES \`city\` (\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  `;