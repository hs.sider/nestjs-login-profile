export const CREATE_COUNTRY_TABLE = `
  CREATE TABLE \`country\` (
    \`id\` int(11) NOT NULL AUTO_INCREMENT,
    \`name\` varchar(45) DEFAULT NULL,
    PRIMARY KEY (\`id\`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  `;

export const POPULATE_COUNTRY_TABLE = `
  INSERT INTO \`country\`
    (\`name\`)
  VALUES
    ('Argentina'),
    ('Bolivia'),
    ('Chile');
  `;