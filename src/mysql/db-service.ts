
import { Logger } from "@nestjs/common";
import { CREATE_ADDRESS_TABLE, CREATE_CITY_TABLE, CREATE_COUNTRY_TABLE, CREATE_PROFILE_TABLE, CREATE_USER_TABLE, POPULATE_CITY_TABLE, POPULATE_COUNTRY_TABLE } from "./query";
import { QueryExecutor } from "./query-executor";

const mysql = require('mysql2/promise');

export class DBService {

    private static readonly logger = new Logger(DBService.name);

    private static _connectionInstance;

    public static get connection() {
        return this._connectionInstance;
    }

    public static async init() {

        this.logger.log(`Connecting to database...`);
        try {
            this._connectionInstance = await mysql.createConnection({
                host: process.env.DATABASE_HOST,
                port: process.env.DATABASE_PORT,
                user: process.env.DATABASE_USER,
                password: process.env.DATABASE_PASSWORD
            });
            const [rows] = await this._connectionInstance.execute(`CREATE DATABASE IF NOT EXISTS ${process.env.DATABASE_DBNAME}`, []);
            await this._connectionInstance.changeUser({ database: process.env.DATABASE_DBNAME });
            if (rows.affectedRows > 0) {
                this.logger.log(`Creating database tables and inserting default data`);
                await QueryExecutor.execute([
                    CREATE_USER_TABLE,
                    CREATE_COUNTRY_TABLE,
                    CREATE_CITY_TABLE,
                    CREATE_ADDRESS_TABLE,
                    CREATE_PROFILE_TABLE,
                    POPULATE_COUNTRY_TABLE,
                    POPULATE_CITY_TABLE
                ])
            }
            this.logger.log(`Connection to the database successfully established`);
        }catch(err) {
            this.logger.error(err);
        }

    }
}
