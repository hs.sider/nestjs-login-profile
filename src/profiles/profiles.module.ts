import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { UsersModule } from 'src/users/users.module';
import { ProfilesController } from './profiles.controller';
import { ProfilesService } from './profiles.service';

@Module({
    controllers: [ProfilesController],
    providers: [ProfilesService],
    exports: [ProfilesService],
    imports: [UsersModule, forwardRef(() => AuthModule)]
})
export class ProfilesModule { }
