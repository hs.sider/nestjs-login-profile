
export class DisplayAddressDto {
    street: string;
    city: string;
    country: string;
}