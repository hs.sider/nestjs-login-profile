import { DisplayAddressDto } from "./displayAddressDto";

export class DisplayProfileDto {
    id: number;
    name: string;
    address: DisplayAddressDto;
}
