
export class CreateProfileDto {
    username: string;
    password: string;
    name: string;
    address: string;
    cityId: number;
}