import { Body, Controller, Get, Param, Post, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ProfileValidationPipe } from 'src/pipes/profile-validation.pipe';
import { CreateProfileDto } from './dto/createProfileDto';
import { DisplayProfileDto } from './dto/displayProfileDto';
import { ProfilesService } from './profiles.service';

@Controller('profiles')
export class ProfilesController {

    constructor(private profilesService: ProfilesService) { }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getProfileByToken(@Request() req) {
        return req.user;
    }

    @Get(':id')
    getProfileById(@Param('id') idProfile: string): Promise<DisplayProfileDto> {
        return this.profilesService.getProfileByUserId(idProfile);
    }

    // @UseGuards(JwtAuthGuard)
    @Post()
    createProfile(@Body(ProfileValidationPipe) createProfileDto: CreateProfileDto): Promise<DisplayProfileDto> {
        return this.profilesService.createProfile(createProfileDto);
    }
}
