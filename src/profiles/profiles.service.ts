import { BadRequestException, ConflictException, Injectable, Logger } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UsersService } from 'src/users/users.service';
import { CreateProfileDto } from './dto/createProfileDto';
import { DisplayProfileDto } from './dto/displayProfileDto';
import { DBService } from 'src/mysql/db-service';
import { DisplayAddressDto } from './dto/displayAddressDto';

@Injectable()
export class ProfilesService {

    private readonly logger = new Logger(ProfilesService.name);
    
    constructor(private usersService: UsersService) { }

    async getProfileByUserId(id: string): Promise<DisplayProfileDto> {

        const query = `
            SELECT profile.id, profile.name, address.street, city.name AS 'city', country.name AS 'country'
            FROM profile
            JOIN address
            ON profile.addressId = address.id
            JOIN city
            ON address.cityId = city.id
            JOIN country
            ON city.countryId = country.id
            WHERE profile.userId = ?;`

        const [rows] = await DBService.connection.execute(query, [id]);

        let address: DisplayAddressDto = new DisplayAddressDto();
        address.street = rows[0].street;
        address.city = rows[0].city;
        address.country = rows[0].country;

        let profile: DisplayProfileDto = new DisplayProfileDto();
        profile.id = rows[0].id;
        profile.name = rows[0].name;
        profile.address = address;

        return profile;
    }

    async createProfile(profile: CreateProfileDto): Promise<DisplayProfileDto> {

        const { username, password, name, address, cityId } = profile;

        const usernameExist = await this.usersService.getUserByUsername(username);
        if (usernameExist) {
            this.logger.warn(`Profile cannot be created because username '${username}' already exist`);
            throw new ConflictException('Username already exist');
        }

        const salt = await bcrypt.genSalt();
        const encriptedPassword = await bcrypt.hash(password, salt);

        try {
            this.logger.log(`Executing query profile insertion`);
            const [addressInserted] = await DBService.connection.execute('INSERT INTO address (cityId, street) VALUES(?,?)', [cityId, address]);
            const [userInserted] = await DBService.connection.execute('INSERT INTO user (username, password) VALUES(?,?)', [username, encriptedPassword]);
            await DBService.connection.execute('INSERT INTO profile (userId, addressId, name) VALUES(?,?,?)', [userInserted['insertId'], addressInserted['insertId'], name]);

            return this.getProfileByUserId(userInserted['insertId']);
        } catch (err) {
            this.logger.warn(err);
            throw new BadRequestException('Profile creation failed');
        }
    }

}
